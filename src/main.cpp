#include "help.cpp"
#include "modules/counter.hpp"
#include "modules/view.hpp"

int main(int argc, char* argv[]) {

    VideoCapturePeopleCounter* counter = new VideoCapturePeopleCounter();
    counter->delegate = new WindowController(counter);
    counter->setRefLineY(120);
    counter->start();
}